#!/usr/bin/python
# X2H
# etree  parsers  sax

from xml.dom.minidom import parse

# Open XML document using minidom parser
DOMTree = parse("movies.xml")
collection = DOMTree.documentElement
if collection.hasAttribute("shelf"):
   print ("Root element : {}".format(collection.getAttribute("shelf")))

# Get all the movies in the collection
movies = collection.getElementsByTagName("movie")

# Print detail of each movie.
for movie in movies:
   print ("*****Movie*****")
   if movie.hasAttribute("title"):
      print ("Title: {}".format(movie.getAttribute("title")))

   type = movie.getElementsByTagName('type')[0]
   print ("Type: {}".format(type.childNodes[0].data))
   format1 = movie.getElementsByTagName('format1')[0]
   print ("Format: {}".format(format1.childNodes[0].data))
   rating = movie.getElementsByTagName('rating')[0]
   print ("Rating: {}".format(rating.childNodes[0].data))
   description = movie.getElementsByTagName('description')[0]
   print ("Description: {}".format(description.childNodes[0].data))
   
'''
task:
create a table with 5 columns
                 MOVIES
Title       | Type        | Format | Rating | Description            |
Enemy behind|War thriller | DVD    | PG     | talk about US-Japan War|

'''