#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

with open("email.txt","r") as email_file:
    email_data = email_file.read()

email_reg = re.compile('[\w.]+@[\w.]+',re.I)
print (email_reg.findall(email_data))