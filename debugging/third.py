#!/usr/bin/python
# -*- coding: utf-8 -*-

def my_add(a,b):
    a = int(a)
    b = int(b)
    c = a + b
    return c

def first():
    print ("addition of two numbers - {}".format(my_add(11,22)))
    second()
    print ("This is my first function")

def second():
    third()
    print ("This is my second function")

def third():
    fourth()
    print ("This is my third function")

def fourth():
    fifth()
    print ("this is my fourth function")

def fifth():
    print ("this is my fifth function")

# main
import pdb
pdb.set_trace()
first()

