#!/usr/bin/python
# -*- coding: utf-8 -*-

import pdb
version = 3.0
def my_add(a,b):
    ''' This is for addition of two numbers and string'''
    a = int(a)
    b = int(b)
    return a + b

def my_sub(a,b):
    ''' This is for substraction of two numbers '''
    if a > b:
        return a - b
    else:
        return b - a
    
def my_multi(a,b):
    ''' This is for multiplication of two numbers '''
    return (a * b)

def my_div(a,b):
    ''' this is for division of two numbers '''
    return (a/b)

## main program
if __name__ == '__main__':    # everything above this line is imported.
                              # everythign below this line is not used during import.
    print ("Welcome to the debugging chapter")
    print ("we dont need to debug every line")
    pdb.set_trace()
    print ("Addition of two numbers is {}".format(my_add(11,22)))
    print ("Substraction of two nubers is {}".format(my_sub(22,33)))
    print ("multiplication of two numbers is {}".format(my_multi(2,11)))
    print ("division of two numbers is {}".format(my_div(22,2)))

