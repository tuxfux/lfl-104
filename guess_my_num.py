#!/usr/bin/python
# -*- coding: utf-8 -*-
# break -> take you abruptly out of the loop(for/while).
# sys.exit -> takes your out of the program.
# http://people.cs.pitt.edu/~alanjawi/cs449/code/shell/UnixSignals.htm
# continue

import sys
# 1) google -> to generate random number everytime.(1-10)
# 2) No of attempts should be restricted to 3.
my_num =  7 # number in my palm
#test = True

yes_no = input("do you want to exit the game - y/n: ")
if yes_no == 'y':
    sys.exit(0)


#while test: # u go into a while loop,if condition is true
while True:
    answer = int(input("please enter your num:"))
    
    if answer > my_num:
        print ("buddy you guessed a larger num")
    elif answer < my_num:
        print ("buddy you guessed a smaller num")
    elif answer == my_num:
        print ("congo!! you guessed the right number")
        #test = False
        break
        
print ("thanks for playing the game")




