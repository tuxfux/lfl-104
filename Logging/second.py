#!/usr/bin/python3
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# man date or    -> for date format.
# http://man7.org/linux/man-pages/man1/date.1.html
# https://docs.python.org/3/library/time.html#time.strftime
## basicConfig doesnt work from spyder.
## need to use the anaconda prompt.

import logging

logging.basicConfig(filename="my_app.txt",filemode="a",
                    level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(name)s -  %(message)s',
                    datefmt='%c')

logging.debug("This is a debug message")
logging.info("This is an INFO message")
logging.warning("This is a Warning message")
logging.error("This is an error message")
logging.critical("This is an critical message")

'''
* basicConfig can only log to a file - local file
* logger name - root
* logging to same file is always a challenge to segertrate the logs.
* logging has to be done to different files to seperate the log.

'''

