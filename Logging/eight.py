#!/usr/bin/python
import logging

logger = logging.getLogger('logger') # logger 
logger.setLevel(logging.CRITICAL)               # filter for logger
logger1 = logging.getLogger('logger1') # logger 
logger1.setLevel(logging.WARNING)               # filter for logger
# create file handler which logs even debug messages
fh = logging.FileHandler('spam.log')         # file handler
fh.setLevel(logging.DEBUG)                   # file filter
# create console handler with a higher log level
ch = logging.StreamHandler()                 # console 
ch.setLevel(logging.ERROR)                   # console handler filter
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger1.addHandler(ch)
logger.addHandler(fh)

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')
logger1.warning('logger1:warn message')
logger1.error('logger1:error message')
logger1.critical('logger1:critical message')