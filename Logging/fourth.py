#!/usr/bin/python3
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# man date or    -> for date format.
# http://man7.org/linux/man-pages/man1/date.1.html
# https://docs.python.org/3/library/time.html#time.strftime
## basicConfig doesnt work from spyder.
## need to use the anaconda prompt.
## crontab or a scheduler
## https://docs.python.org/3/library/subprocess.html

import logging
from subprocess import Popen,PIPE
import shlex
import re

logging.basicConfig(filename="my_disk.txt",filemode="a",
                    level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(name)s -  %(message)s',
                    datefmt='%c')

#command = shlex.split("df -h /")
p1 = Popen(shlex.split("df -h /"),stdout=PIPE)
p2 = Popen(shlex.split("tail -n 1"),stdin=p1.stdout,stdout=PIPE)
output = (p2.communicate()[0]).decode(encoding="UTF-8") # bytes to str
disk_space = int(re.search('([0-9]*)%',output,re.I).group(1))

## df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g'
## main
if __name__ == '__main__':
    
    if disk_space < 50:    
        logging.info("The disk space looks good - {}".format(disk_space))
    elif disk_space < 70:        
        logging.warning("The disk is getting filled up - {}".format(disk_space))
    elif disk_space < 80:
         logging.error("The disk is filled up now - {}".format(disk_space))
    else:
        logging.critical("The Application is gone down - {}".format(disk_space))

"""
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ dd if=/dev/zero of=bigfile count=1 bs=1G
1+0 records in
1+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 8.55909 s, 125 MB/s
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ du -sh bigfile
1.0G    bigfile
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ df -h /
Filesystem      Size  Used Avail Use% Mounted on
rootfs          466G   91G  376G  20% /
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ dd if=/dev/zero of=bigfile1 count=1 bs=1G
1+0 records in
1+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 8.62364 s, 125 MB/s
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ df -h /
Filesystem      Size  Used Avail Use% Mounted on
rootfs          466G   92G  375G  20% /
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ dd if=/dev/zero of=bigfile2 count=3 bs=1G
3+0 records in
3+0 records out
3221225472 bytes (3.2 GB, 3.0 GiB) copied, 15.6352 s, 206 MB/s
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ df -h /
Filesystem      Size  Used Avail Use% Mounted on
rootfs          466G   95G  372G  21% /
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ ./fourth.py
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104/Logging$ 
"""