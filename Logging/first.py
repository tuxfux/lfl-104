#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

logging.debug("This is a debug message")
logging.info("This is an INFO message")
logging.warning("This is a Warning message")
logging.error("This is an error message")
logging.critical("This is an critical message")

"""
WARNING:root:This is a Warning message
ERROR:root:This is an error message
CRITICAL:root:This is an critical message

DEBUG < INFO < WARNING < ERROR < CRITICAL

The default level is WARNING, which means that only 
events of this level and above will be tracked, 
unless the logging package is configured to do otherwise.

"""