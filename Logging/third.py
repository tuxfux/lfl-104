#!/usr/bin/python3
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# man date or    -> for date format.
# http://man7.org/linux/man-pages/man1/date.1.html
# https://docs.python.org/3/library/time.html#time.strftime
## basicConfig doesnt work from spyder.
## need to use the anaconda prompt.

import logging

logging.basicConfig(filename="my_disk.txt",filemode="a",
                    level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(name)s -  %(message)s',
                    datefmt='%c')

## main
if __name__ == '__main__':
    disk_space = int(input(" Enter you disk space:"))
    
    if disk_space < 50:    
        logging.info("The disk space looks good - {}".format(disk_space))
    elif disk_space < 70:        
        logging.warning("The disk is getting filled up - {}".format(disk_space))
    elif disk_space < 80:
         logging.error("The disk is filled up now - {}".format(disk_space))
    else:
        logging.critical("The Application is gone down - {}".format(disk_space))