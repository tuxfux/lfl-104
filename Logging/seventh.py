#!/usr/bin/python3
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# man date or    -> for date format.
# http://man7.org/linux/man-pages/man1/date.1.html
# https://docs.python.org/3/library/time.html#time.strftime
## basicConfig doesnt work from spyder.
## need to use the anaconda prompt.
## crontab or a scheduler
## https://docs.python.org/3/library/subprocess.html

import logging
from logging.handlers import SysLogHandler
from subprocess import Popen,PIPE
import shlex
import re

#logging.basicConfig(filename="my_disk.txt",filemode="a",
#                    level=logging.DEBUG,
#                    format='%(asctime)s - %(levelname)s - %(name)s -  %(message)s',
#                    datefmt='%c')

#
#Loggers expose the interface that application code directly uses.
# ex: basicConfig -> default logger is root.
# your logger defines the name of the event generated.
#Handlers send the log records (created by loggers) to the appropriate destination.
# ex: filename="my_disk.txt",filemode="a"
# https://docs.python.org/3/howto/logging.html#useful-handlers
#Filters provide a finer grained facility for determining which log records to output.
# ex: level=logging.DEBUG
#Formatters specify the layout of log records in the final output.
# ex: format='%(asctime)s - %(levelname)s - %(name)s -  %(message)s'
#

# create logger
logger = logging.getLogger('RootDisk') # logger names RootDisk
logger.setLevel(logging.DEBUG)         # filter at logger level

# create console handler and set level to debug
ch = SysLogHandler(address="/dev/log")      # handler
ch.setLevel(logging.DEBUG)        # filter at handler level

# create formatter
formatter = logging.Formatter(' -  %(name)s - %(levelname)s - %(message)s')

## how to co-relate logger,handler and formatter
# add formatter to ch
ch.setFormatter(formatter)  # handler and formatter

# add ch to logger
logger.addHandler(ch)    # logger and handler

#command = shlex.split("df -h /")
p1 = Popen(shlex.split("df -h /"),stdout=PIPE)
p2 = Popen(shlex.split("tail -n 1"),stdin=p1.stdout,stdout=PIPE)
output = (p2.communicate()[0]).decode(encoding="UTF-8") # bytes to str
disk_space = int(re.search('([0-9]*)%',output,re.I).group(1))

## df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g'
## main
if __name__ == '__main__':

    if disk_space < 50:
        logger.info("The disk space looks good - {}".format(disk_space))
    elif disk_space < 70:
        logger.warning("The disk is getting filled up - {}".format(disk_space))
    elif disk_space < 80:
         logger.error("The disk is filled up now - {}".format(disk_space))
    else:
        logger.critical("The Application is gone down - {}".format(disk_space))

'''
import logging.handlers as lh

print (dir(lh))
['BaseRotatingHandler', 'BufferingHandler', 'DEFAULT_HTTP_LOGGING_PORT', 'DEFAULT_SOAP_LOGGING_PORT', 'DEFAULT_TCP_LOGGING_PORT', 'DEFAULT_UDP_LOGGING_PORT', 'DatagramHandler', 'HTTPHandler', 'MemoryHandler', 'NTEventLogHandler', 'QueueHandler', 'QueueListener', 'RotatingFileHandler', 'SMTPHandler', 'ST_DEV', 'ST_INO', 'ST_MTIME', 'SYSLOG_TCP_PORT', 'SYSLOG_UDP_PORT', 'SocketHandler', 'SysLogHandler', 'TimedRotatingFileHandler', 'WatchedFileHandler', '_MIDNIGHT', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'logging', 'os', 'pickle', 'queue', 're', 'socket', 'struct', 'threading', 'time']
'''
