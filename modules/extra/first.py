#!/usr/bin/python
# -*- coding: utf-8 -*-

version = 3.0
def my_add(a,b):
    ''' This is for addition of two numbers and string'''
    return a + b

def my_sub(a,b):
    ''' This is for substraction of two numbers '''
    if a > b:
        return a - b
    else:
        return b - a
    
def my_multi(a,b):
    ''' This is for multiplication of two numbers '''
    return (a * b)

def my_div(a,b):
    ''' this is for division of two numbers '''
    return (a/b)

## main program
if __name__ == '__main__':    # everything above this line is imported.
                              # everythign below this line is not used during import.
    print ("Ignite the bomb")

