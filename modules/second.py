#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0,'C:\\Users\\tuxfux\\Documents\\lfl-python104\\modules\\extra')
import first

def my_add(a,b):
    a = int(a)
    b = int(b)
    return a + b

# main program
if __name__ == '__main__':
    print ("Addition of two numbers is {}".format(my_add(11,22)))
    print ("Addition of two string is {}".format(first.my_add("linux"," rocks")))

