python.org (main web page)

Python
 + 2.x (2020-jan -> 2.x)
 + 3.x 
 
# Installation

Ubuntu
# sudo apt-get install python (2.x)
# sudo apt-get install python3 (3.x)

## reference:
# https://ipython.org/
# https://bpython-interpreter.org/

# sudo apt-get install ipython  (2.x)
# sudo apt-get install ipython3 (3.x)
# sudo apt-get install bpython (2.x)
# sudo apt-get install bpython3 (3.x)


tuxfux@DESKTOP-8B46PBJ:/mnt/c/Windows/system32$ python
Python 2.7.15+ (default, Oct  7 2019, 17:39:04)
[GCC 7.4.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> print "hello world"
hello world
>>>
>>> quit()
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Windows/system32$
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Windows/system32$ python3
Python 3.6.9 (default, Nov  7 2019, 10:44:02)
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print ("hello world")
hello world
>>>
>>> quit()
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Windows/system32$

Windows
https://www.anaconda.com/distribution/#windows

IDE
---
https://stackoverflow.com/questions/81584/what-ide-to-use-for-python






