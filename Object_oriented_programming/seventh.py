#!/usr/bin/python
# -*- coding: utf-8 -*-
# we have to create an new excpetion

# Exception is a parent class
# InvalidAgeException is our child class

class InvalidAgeException(Exception):
    def __init__(self,age):
        self.age = age

def validate_age(age):
    if age >= 18:
        return ("Buddy!! you have come to age!!!")
    else:
        raise InvalidAgeException(age)


# main
age = int(input("please enter you age:"))

try:
    validate_age(age)
#except Exception as e:
#    print ("Buddy!! you still need to grow - {}".format(e.age))
except InvalidAgeException:
    print ("Buddy!! you still need to grow - {}".format(age))
else:
    print (validate_age(age))
finally:
    pass

