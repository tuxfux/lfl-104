reference: https://python.swaroopch.com/oop.html

Programming
- modular based  programming
- Object oriented based programming
APIE

* what is object oriented programming?
* why do we need object oriented programming?
* benefits or object oriented programming?

A - Abstraction


way of defining an object

class is a mix of variable and function as a single entity.
- -> variables/data
+ -> functions/methods

ex:
classification - class/blueprints
bank account
 - account balance
 - interest
 - balance
 + withdraw
 + deposit
 
objects:
 - santosh account
 - satya account
 - swetha account

ex:
classification - class/blueprints
car
 - engine - horse power
 + speed - acceleration
 + brake - decrease the speed
 - tyres(4)
 - body
 - color
 
model - objects
- honda
- bmw
- audi
- jaguar
- nano
- hyundai

class:
ex: villa
- 4 bedrooms
- swimming pool
- car parking

objects
- house1 - pink
- house2 - yellow
- house3 - saffron
- house4 - green

python demo
ex:


In [1]: a = "python"

In [2]: print (type(a))
<class 'str'>

In [3]: #class str

In [4]: # a is a object created out of the str class 'str'

In [5]:

P - Polymorphism

Poly - Many
morphism - forms

ex:

satya
 - son
 - father
 - brother
 - husband
 - employee
 - employer

interest
- educational loan interest
- home loan interest
- personal loan
- gold loan
 
## python stuff

In [7]: a = 10

In [8]: b = 20

In [9]: type(a)
Out[9]: int

In [10]: type(b)
Out[10]: int

In [11]: a + b
Out[11]: 30

In [12]: a = "linux"

In [13]: b = " rocks"

In [14]: type(a)
Out[14]: str

In [15]: type(b)
Out[15]: str

In [16]: a + b
Out[16]: 'linux rocks'

In [17]: a + 2
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-17-ead3c2111b4d> in <module>
----> 1 a + 2

TypeError: can only concatenate str (not "int") to str

In [18]:

#In [1]: # 1/2 + 1/3  = 5/6
#
#In [2]:
#
#In [2]: 1/2 + 1/3
#Out[2]: 0.8333333333333333
#
#In [3]: int(1/2) + int(1/3)
#Out[3]: 0                                                                                                                


In [8]: a = 1

In [9]: b = 1

In [10]: print (dir(a))
['__abs__', '__add__', '__and__', '__bool__', '__ceil__', '__class__', '__delattr__', '__dir__', '__divmod__', '__doc__', '__eq__', '__float__', '__floor__', '__floordiv__', '__format__', '__ge__', '__getattribute__', '__getnewargs__', '__gt__', '__hash__', '__index__', '__init__', '__init_subclass__', '__int__', '__invert__', '__le__', '__lshift__', '__lt__', '__mod__', '__mul__', '__ne__', '__neg__', '__new__', '__or__', '__pos__', '__pow__', '__radd__', '__rand__', '__rdivmod__', '__reduce__', '__reduce_ex__', '__repr__', '__rfloordiv__', '__rlshift__', '__rmod__', '__rmul__', '__ror__', '__round__', '__rpow__', '__rrshift__', '__rshift__', '__rsub__', '__rtruediv__', '__rxor__', '__setattr__', '__sizeof__', '__str__', '__sub__', '__subclasshook__', '__truediv__', '__trunc__', '__xor__', 'bit_length', 'conjugate', 'denominator', 'from_bytes', 'imag', 'numerator', 'real', 'to_bytes']

In [11]: a + b
Out[11]: 2

In [12]: type(a)
Out[12]: int

In [13]: a.__add__(b)
Out[13]: 2

I - Inheritance

parents
- mom
  - blonde hair
  - blue eyes
  - wheatish
  - doctor
- dad
  - brown eyes
  - brown hair
  - dark complexion
  - Architect

kid(mom,dad)
 - blue eyes(mom)
 - black hair
 - dark complexion(dad)
 - cook

## python
E - Encapsulation
hiding the data.Dealing with data underlying module within a class or a subclass.
we are not directly dealing with the data/variable.


bank
cashier - swetha
friend - satya
   Account
     - balance
     + deposit()
     + withdraw()

data(variable)
- public
- private
- protected
















