#!/usr/bin/python
# -*- coding: utf-8 -*-
# concepts
# what is type keyword in python3/python2
# Any variable you define within a class is a data.
# Any function you define within a class is called as method.
# class can be also a type.

# class/blueprint/super class
class Account:
    balance = 0 # data
    def my_balance(self):   # method
        return ("my balance is {}".format(self.balance))
        
    
print (Account,type(Account)) # <class '__main__.Account'> <class 'type'>

## instance/object
## instance or object is a replica of your class.
## your instance is a individual entity

## object/instance
rupesh = Account()
print (rupesh,type(rupesh)) 
# <__main__.Account object at 0x000001BB87085780> <class '__main__.Account'>
# a = 10
# print (type(a)) - <class 'int'>
# a is an object/instance of class int.

## rupesh is an object/instance of class Account/__main__.Account

print (Account,type(Account))
# Account is class and of type - type.

##
# Lets check balance of rupesh
print (dir(rupesh))
print (rupesh.balance)
#print (rupesh.my_balance())
#TypeError: my_balance() takes 0 positional arguments but 1 was given
#class Account:
#    balance = 0 # data
#    def my_balance():   # method
#        return ("my balance is {}".format(balance))

###
# update the function defination with self.
#    def my_balance(self):   # method
#        return ("my balance is {}".format(balance))
# self basically is referring to the instance/object called rupesh.
# self is not a keyword its just a variable , self -> rupesh

# rupesh is passed as a argument while calling it as rupesh.my_balance
#print (rupesh.my_balance())
#Traceback (most recent call last):
#  File "third.py", line 53, in <module>
#    print (rupesh.my_balance())
#  File "third.py", line 13, in my_balance
#    return ("my balance is {}".format(balance))
#NameError: name 'balance' is not defined

### class variables and instance variables
## we can have access to the class and also can access as instance


print ("Account balance - {}".format(Account.balance))  # 0 is a values from class variable balance
print ("rupesh balance - {}".format(rupesh.balance)) # data from a instance/object variable
rupesh.balance = 1000
print ("rupesh balance - {}".format(rupesh.balance))   # instance variable
print ("Account balance - {}".format(Account.balance)) # class variable
Account.balance = 2000
print ("rupesh balance - {}".format(rupesh.balance))   # instance variable
print ("Account balance - {}".format(Account.balance)) # class variable

## working with method within a function
print (rupesh.my_balance())

## satya
satya = Account()
print ("satya balance - {}".format(satya.balance))   # instance variable
print ("Account balance - {}".format(Account.balance)) # class variable
print (satya.my_balance())



