#!/usr/bin/python
# -*- coding: utf-8 -*-
# requirment : pass a name as a string - "santosh kumar"
# object is a master class for all classes
# @classmethod - nothing but a duplicate constuctor or a another construtor method implemenation.
# @staticmethod - General method which you bundle into class.
# https://www.toptal.com/python/python-design-patterns


## case i: FUNCTION IS OUTSIDE
#def is_full_name(name_str):
#    name = name_str.split()
#    return len(name) > 1   # if the lenght is greater than 1


# class Student(object):
class Student:
    def __init__(self,first_name,second_name): # constructor
        self.first_name = first_name
        self.second_name = second_name
        
#    def from_newstring(self,name_str):
#        first_name,last_name = name_str.split()
#        return first_name,last_name
    
    @classmethod                              
    def from_string(cls,name_str):             # class method constructor
        first_name,last_name = name_str.split()
        student = cls(first_name,last_name)
        return student
    
    @classmethod
    def from_comma(cls,name_str):             # class method construtor
        first_name,last_name = name_str.split(',')
        student = cls(first_name,last_name)
        return student
    
    @staticmethod
    def is_full_name(name_str):
        name = name_str.split()
        return len(name) > 1   # if the lenght is greater than 1
        
    def student_details(self):
        return ("The student details are - {} {}".format(self.first_name,self.second_name))
    
    
# Main
student1 = Student("santosh","kumar")  # __init__ constructor is getting called.
print (student1.student_details())



student2 = Student.from_string("santosh kumar") # method called via class
print (student2.student_details())              # method called via instance/object
print (student2.is_full_name("santosh kumar"))

student3 = Student.from_comma("santosh,kumar")
print (student3.student_details())
print (student3.is_full_name("santosh,kumar"))

#student3 = Student("swetha","kumari")
#print (student3.from_newstring("swetha kumari"))

## when my function is outside the class
#print (is_full_name("santosh kumar venkatesh prasad"))
#print (is_full_name("santosh kumar"))
