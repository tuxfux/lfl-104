#!/usr/bin/python
# -*- coding: utf-8 -*-

balance = 0

def withdraw():
    global balance
    balance = balance - 1000
    return balance

def deposit():
    global balance
    balance = balance + 5000
    return balance

## satya
print ("balance of satya is {}".format(balance))
deposit()
withdraw()
print ("balance of satya after transation {}".format(balance))

## swetha
print ("balance of swetha is {}".format(balance))
deposit()
withdraw()
print ("balance of swetha after transation {}".format(balance))