#!/usr/bin/python
# -*- coding: utf-8 -*-
# lenght is getting added as negative.
# I dont want people to add the lenght directly as a variable.

class Area:
    def __init__(self,length=1):
        self.length = length
    
    def compute_area(self):
        my_area = self.length * self.length
        return (my_area)

## Main
A = Area(10)
print (A.length)
A.length = 20
print (A.length)
print (A.compute_area())
A.length = -1000
print (A.compute_area())