#!/usr/bin/env python
# -*- coding: utf-8 -*-
# reference : http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
# left over of function kingdom.


## defining my decorator
def rennovate(func):
    def newfunc(*args,**kwargs):
        try:
            func(*args,**kwargs)
        except Exception as e:
            return ("we hit on error - {}".format(e))
        else:
            return func(*args,**kwargs)
        finally:
            pass
    return newfunc
            

## defination
@rennovate
# my_add = rennovate(my_add)  # old ways
def my_add(a,b):
    return a + b

@rennovate
#my_add = rennovate(my_add)  # old ways
def my_div(a,b):
    return a/b

@rennovate
# add_three = rennovate(add_three) # old ways
def add_three(a,b,c):
    return a + b + c

# wrap my current functions
# old way
#my_add = rennovate(my_add)
#print (my_add,type(my_add))
#my_div = rennovate(my_div)


# main

print (my_add(11,22)) # *args
print (my_div(22,11)) # *args
print (my_add(a=11,b=22)) # **kwargs
print (my_div(a=22,b=11)) # **kwargs
print (my_add(11,'a'))
print (my_div(22,0))
print (add_three(11,22,33))
print (add_three(a=11,b=22,c=33))
print (add_three(a=11,b=22,d=33))





"""
## case I
def my_add(a,b):
    try:
        a = int(a)
        b = int(b)
    except Exception as e:
        return ("we hit on error - {}".format(e))
    else:
        return a + b
    finally:
        pass

def my_div(a,b):
    try:
        a/b
    except Exception as e:
        return ("we hit on error - {}".format(e))     
    else:
        return a/b
    finally:
        pass


## main
print (my_add(11,22)) # *args
print (my_div(22,11)) # *args
print (my_add(a=11,b=22)) # **kwargs
print (my_add(a=22,b=11)) # **kwargs

print (my_add(11,'a'))
print (my_div(22,0))
"""