#!/usr/bin/python
# -*- coding: utf-8 -*-
# lenght is getting added as negative.
# I dont want people to add the lenght directly as a variable.
#  private - that nobody should be able to access it from outside the class

class Area:
    def __init__(self,length=1):
        self.__length = length
    
    def compute_area(self):
        my_area = self.__length * self.__length
        return (my_area)
    
    ## implement the getter method
    @property
    def length(self):
        print ("Implementation of the @property method")
        return (self.__length)
    
    ## implemenation of the setter method
    @length.setter
    def length(self,value):
        print ("Implemenation of the @setter method")
        if value < 0:
            raise ValueError("Length cannot be less than zero.")
        self.__length = value
        
        
# main    
A = Area(10) #  A = Area(length=10)
print (A.length)
A.length = 20
print (A.length)
print (A.compute_area())
A.length = -1000
print (A.compute_area())