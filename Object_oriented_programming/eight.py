#!/usr/bin/python
# -*- coding: utf-8 -*-
# __<something>__ -> dunders or magic methods
# https://rszalski.github.io/magicmethods/
# 1/2 + 1/3 = 5/6
# operator overloading

class RationalNumber:
    def __init__(self,numerator,denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self,other):
        # for the object which is not part of rational number class.
        if not isinstance(other, RationalNumber):
             other = RationalNumber(other)
        # taking care of adding two rational number
        n = self.n * other.d + self.d * other.n # (1*3 + 1*2)
        d = self.d * other.d # 3 * 2
        
        return RationalNumber(n,d)  # instance of the RationalNumber class
                                    # return object is always passed as __str__
                                    
    def __str__(self):
        return ("{}/{}".format(self.n,self.d))
    
    __repr__ = __str__



# main

## both my objects are rational objects
a = RationalNumber(1,2) # a.n = 1 ,a.d = 2
b = RationalNumber(1,3) # b.n = 1, b.d = 3
print (a + b)  # a.__add__(b)  # b => other
               # a.__add__(b) -> self.n -> a.n => other.n => b.n

# one of my object is rational objects and other is interger
g = RationalNumber(1,2) # g.n = 1, g.d = 2 -> 1/2
h = 2                   # h.n = 2, h.d = 1 -> 2/1
print (g + h) # print (g.__add__(h)) -> 1/2 + 2/1 = 5/2

## both my objects are integers
c = 10  # c is a interger type
d = 20  # d is also integer type
print (c + d)

## both of objects are strings
e = "python"
f = " rocks"
print (e + f)

'''
In [5]: a = 10

In [6]: type(a)
Out[6]: int

In [7]: isinstance(a,int)
Out[7]: True

In [8]: b = "python"

In [9]: isinstance(b,int)
Out[9]: False
'''