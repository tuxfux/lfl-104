#!/usr/bin/python
# -*- coding: utf-8 -*-

def account():
    return {'balance':0}

def withdraw(account):
    account['balance'] = account['balance'] - 1000
    return account['balance']

def deposit(account):
    account['balance'] = account['balance'] + 5000
    return account['balance']

## account
satya = account()
print (satya,type(satya))
swetha = account()

## satya
print ("balance of satya is {}".format(satya['balance']))
deposit(satya)
withdraw(satya)
print ("balance of satya after transation {}".format(satya['balance']))

## swetha
print ("balance of swetha is {}".format(swetha['balance']))
deposit(swetha)
withdraw(swetha)
withdraw(swetha)
print ("balance of swetha after transation {}".format(swetha['balance']))