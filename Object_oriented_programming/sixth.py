#!/usr/bin/python
# __init__ is a special method
# __init__ is called also as a constructor.
# This is the first function which gets called when you create a instance.

## bank
## zero balance accounts
class Account:                   # class
    def __init__(self,amount):  # constructor
        self.balance = amount   # data/variable
    def deposit(self,amount):           # method 
        self.balance = self.balance + amount
    def withdraw(self,amount):          # method
        self.balance = self.balance - amount
    def my_balance(self):        # method
        return ("my balance is : {}".format(self.balance))

# child class of Account
# withdraw is polymorphic
# deposit(),my_balance() method are inherited.
class MinBalanceAccount(Account):
    def __init__(self,amount):
        Account.__init__(self,amount)  # inherit all a variable from account class
                                # to my child class
    def withdraw(self,amount):
        if self.balance - amount < 1000:
            print ("Buddy!! time to call you daddy")
        else:
            Account.withdraw(self,amount)
    
# Account defincation
# swetha salaried
# over draft
swetha = Account(0) # instance/objects

# Logs of swetha
print ("initial balance of swetha - {}".format(swetha.balance))
swetha.deposit(5000)
swetha.withdraw(1000)
swetha.withdraw(5000)
print (swetha.my_balance())

# balance => -1000
# canara_swetha = MinBalanceAccount(-1000)


## rupesh
## Mtech - neural networks
## no over draft
## minimal balance account 1000 rs.
## __init__ under the parent class is not inherited explicitly.
rupesh = MinBalanceAccount(1000)
#print (dir(rupesh))
print ("initial balance of rupesh - {}".format(rupesh.balance))
rupesh.deposit(5000)
print (rupesh.my_balance())
rupesh.withdraw(3000)
print (rupesh.my_balance())
rupesh.withdraw(2000)
print (rupesh.my_balance())
rupesh.withdraw(500)
print (rupesh.my_balance())
