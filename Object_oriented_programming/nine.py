#!/usr/bin/python
# -*- coding: utf-8 -*-
# Encapsulation - hinding you data
# reference 
# http://bryanrunck.com/blog/2017/2/25/encapsulation-in-python-reflections-from-linked-lists-implementation
# https://radek.io/2011/07/21/private-protected-and-public-in-python/
# public - available to everyone
# private - that nobody should be able to access it from outside the class
# protected - accessible only from within the class and it’s subclasses

"""
# private

In [15]: class Cup:
    ...:     def __init__(self):
    ...:         self._color = None # protected
    ...:         self.__content = None # private
    ...:
    ...:     def fill(self, beverage):
    ...:         self.__content = beverage
    ...:
    ...:     def empty(self):
    ...:         self.__content = None


In [8]: red = Cup()

In [9]: print (dir(red))
['_Cup__content', '__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_color', 'empty', 'fill']

In [10]: red._Cup_content
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
<ipython-input-10-f809b3bf8499> in <module>
----> 1 red._Cup_content

AttributeError: 'Cup' object has no attribute '_Cup_content'

In [11]: red._Cup__content

In [12]: red.fill("cofee")

In [13]: red.__content
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
<ipython-input-13-1ff2ab819901> in <module>
----> 1 red.__content

AttributeError: 'Cup' object has no attribute '__content'

In [14]: red._Cup__content
Out[14]: 'cofee'


# protected
class Cup:
    def __init__(self):
        self.color = None
        self._content = None # protected

    def fill(self, beverage):
        self._content = beverage

    def empty(self):
        self._content = None

## nothing changes for us. As protected is a convention in python.
green = Cup()
green.color = "green"
green._content = "tea"

print (green.color)
print (green._content)

green.fill("coffee")
print (green._content)
green.empty()
print (green._content)


## public
class Cup:
    def __init__(self):
        self.color = None
        self.content = None

    def fill(self, beverage):
        self.content = beverage

    def empty(self):
        self.content = None
        

green = Cup()
green.color = "green"
green.content = "tea"

print (green.color)
print (green.content)

green.fill("coffee")
print (green.content)
green.empty()
print (green.content)

"""
