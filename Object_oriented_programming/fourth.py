#!/usr/bin/python
# __init__ is a special method
# __init__ is called also as a constructor.
# This is the first function which gets called when you create a instance.

class Account:
    def __init__(self):
        self.balance = 0
    def deposit(self):
        self.balance = self.balance + 5000
    def withdraw(self):
        self.balance = self.balance - 1000
    def my_balance(self):
        return ("my balance is - {}".format(self.balance))
    
# Account defincation
swetha = Account() # instance/objects
satya = Account()
rupesh = Account()

# Logs of swetha
print ("initial balance of swetha - {}".format(swetha.balance))
swetha.deposit()
swetha.withdraw()
print (swetha.my_balance())

# Logs of satya
print ("initial balance of satya - {}".format(satya.balance))
satya.deposit()
satya.withdraw()
satya.withdraw()
print (satya.my_balance())

# rupesh
print ("initial balance of rupesh - {}".format(rupesh.balance))
rupesh.deposit()
rupesh.deposit()
rupesh.withdraw()
print (rupesh.my_balance())



