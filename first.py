#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author : santosh
# Usage: want to say hi to everyone.
# 04-03-2020 - i created this program
#            - 

print ("hello world!!")



'''

1) User convience we have .py file - windows user.
2) Platform independent.
3) Modular programming - using function of one program in another program.
4) shabang is must for linux based enviornments.


tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ dir
Day01-Setting_up_python.txt  Day02-Introduction_and_help.txt  Myfirstnotes.ipynb  first.py
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ python first.py
hello world!!
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$    

tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ perl first.py
hello world!!tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ python3 first.py
hello world!!
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ python first.py
hello world!!

## executable

tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ ls -l
total 12
-rwxrwxrwx 1 tuxfux tuxfux 1176 Mar  2 19:09 Day01-Setting_up_python.txt
-rwxrwxrwx 1 tuxfux tuxfux   32 Mar  4 18:39 Day02-Introduction_and_help.txt
-rwxrwxrwx 1 tuxfux tuxfux  813 Mar  2 19:07 Myfirstnotes.ipynb
-rwxrwxrwx 1 tuxfux tuxfux  928 Mar  4 18:59 first.py
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ ./first.py
./first.py: line 2: $'\r': command not found
./first.py: line 3: syntax error near unexpected token `"hello world!!"'
'/first.py: line 3: `print ("hello world!!")
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$  

#!/usr/bin/python3
put it always at the beginning of the line.
shabang

# -> hash -> sha
! -> exclamation -> bang  


tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ which python
/usr/bin/python
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ which python3
/usr/bin/python3
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$ ./first.py
hello world!!
tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python104$                                                                                                                                                                                                                                                      

'''