# dictionaries are usually unique objects
# hashes/hash/key-value/dict/dictionary


# aadhar 
# SSN
# employee/students

001,swetha,db engineer

002,satya, db manager

003,satya, head of sales

# dictionary
- json
- yaml

In [24]: emp_db = {
    ...: '001':'swetha',
    ...: '002':'satya',
    ...: '003':'satya'}

In [25]: print (emp_db,type(emp_db))
{'001': 'swetha', '002': 'satya', '003': 'satya'} <class 'dict'>

In [26]: emp_db['001']
Out[26]: 'swetha'

In [27]: emp_db['002']
Out[27]: 'satya'

In [28]: emp_db['003']
Out[28]: 'satya'

In [29]: ## key and value pairs - dont have anymore indexing associated

In [30]: ## creation of dictionaries

In [31]: my_empty = {}

In [32]: print (my_empty,type(my_empty))
{} <class 'dict'>

In [33]: my_empty = dict()

In [34]: print (my_empty,type(my_empty))
{} <class 'dict'>

In [35]: 


## cheat sheet
list -> ['swetha','satya','satya'],[],list()
tuple -> ('swetha','satya','swetha'),(),tuple()
dict -> {'001': 'swetha', '002': 'satya', '003': 'satya'},{},dict()


In [36]: # insert values into a dictionary

In [37]: emp_db
Out[37]: {'001': 'swetha', '002': 'satya', '003': 'satya'}

In [38]: emp_db['004'] = ['tuxfux','trainer','python']

In [39]: emp_db
Out[39]: 
{'001': 'swetha',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python']}

In [40]: 

In [36]: # insert values into a dictionary

In [37]: emp_db
Out[37]: {'001': 'swetha', '002': 'satya', '003': 'satya'}

In [38]: emp_db['004'] = ['tuxfux','trainer','python']

In [39]: emp_db
Out[39]: 
{'001': 'swetha',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python']}

In [40]: emp_db['001'] = "swaroopa"

In [41]: emp_db
Out[41]: 
{'001': 'swaroopa',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python']}

In [42]: ## keys has to be unique

In [43]: ## overwrite the key values
    ...: 

In [44]: 

In [45]: # functions of dictionary

In [46]: #emp_db.keys

In [47]: emp_db.keys?
Docstring: D.keys() -> a set-like object providing a view on D's keys
Type:      builtin_function_or_method

In [48]: emp_db.keys()
Out[48]: dict_keys(['001', '002', '003', '004'])

In [49]: # emp_db.values

In [50]: emp_db.values?
Docstring: D.values() -> an object providing a view on D's values
Type:      builtin_function_or_method

In [51]: emp_db.values()
Out[51]: dict_values(['swaroopa', 'satya', 'satya', ['tuxfux', 'trainer', 'python']])

In [52]: # emp_db.items

In [53]: emp_db.items?
Docstring: D.items() -> a set-like object providing a view on D's items
Type:      builtin_function_or_method

In [54]: emp_db.items()
Out[54]: dict_items([('001', 'swaroopa'), ('002', 'satya'), ('003', 'satya'), ('004', ['tuxfux', 'trainer', 'python'])])

In [55]: 

In [59]: #emp_db.get

In [60]: emp_db.get?
Signature: emp_db.get(key, default=None, /)
Docstring: Return the value for key if key is in the dictionary, else default.
Type:      builtin_function_or_method

In [61]: emp_db.get('001')
Out[61]: 'swaroopa'

In [62]: emp_db.get('005')

In [63]: print (emp_db.get('005'))
None

In [64]: emp_db['001']
Out[64]: 'swaroopa'

In [65]: emp_db['005']
Traceback (most recent call last):

  File "<ipython-input-65-3eb4abf96417>", line 1, in <module>
    emp_db['005']

KeyError: '005'


In [66]: 


In [67]: #emp_db.update

In [68]: emp_db.update?
Docstring:
D.update([E, ]**F) -> None.  Update D from dict/iterable E and F.
If E is present and has a .keys() method, then does:  for k in E: D[k] = E[k]
If E is present and lacks a .keys() method, then does:  for k, v in E: D[k] = v
In either case, this is followed by: for k in F:  D[k] = F[k]
Type:      builtin_function_or_method

In [69]: emp_db.update({'005':'rupesh','006':'nag'})

In [70]: emp_db
Out[70]: 
{'001': 'swaroopa',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python'],
 '005': 'rupesh',
 '006': 'nag'}
 
## emp_db.fromkeys

In [72]: emp_db.fromkeys?
Signature: emp_db.fromkeys(iterable, value=None, /)
Docstring: Create a new dictionary with keys from iterable and values set to value.
Type:      builtin_function_or_method

In [74]: dict.fromkeys(emp_db)
Out[74]: {'001': None, '002': None, '003': None, '004': None, '005': None, '006': None}

In [75]: dict.fromkeys(emp_db,"IBM")
Out[75]: 
{'001': 'IBM',
 '002': 'IBM',
 '003': 'IBM',
 '004': 'IBM',
 '005': 'IBM',
 '006': 'IBM'}

In [76]: emp_db.fromkeys(emp_db,"IBM")
Out[76]: 
{'001': 'IBM',
 '002': 'IBM',
 '003': 'IBM',
 '004': 'IBM',
 '005': 'IBM',
 '006': 'IBM'}

In [77]:

In [78]: emp_db
Out[78]: 
{'001': 'swaroopa',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python'],
 '005': 'rupesh',
 '006': 'nag'}

In [79]: emp_ibm_db = emp_db.fromkeys(emp_db,"IBM")

In [80]: emp_ibm_db
Out[80]: 
{'001': 'IBM',
 '002': 'IBM',
 '003': 'IBM',
 '004': 'IBM',
 '005': 'IBM',
 '006': 'IBM'}

In [81]: emp_ibm_db = emp_db.fromkeys(emp_db,["ibm","ex at & t"])

In [82]: emp_ibm_db
Out[82]: 
{'001': ['ibm', 'ex at & t'],
 '002': ['ibm', 'ex at & t'],
 '003': ['ibm', 'ex at & t'],
 '004': ['ibm', 'ex at & t'],
 '005': ['ibm', 'ex at & t'],
 '006': ['ibm', 'ex at & t']}
 
# emp_db.setdefault

 In [84]: emp_db.setdefault?
Signature: emp_db.setdefault(key, default=None, /)
Docstring:
Insert key with a value of default if key is not in the dictionary.

Return the value for key if key is in the dictionary, else default.
Type:      builtin_function_or_method

In [85]: emp_db
Out[85]: 
{'001': 'swaroopa',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python'],
 '005': 'rupesh',
 '006': 'nag'}

In [86]: emp_db.setdefault('001',"swetha")
Out[86]: 'swaroopa'

In [87]: emp_db
Out[87]: 
{'001': 'swaroopa',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python'],
 '005': 'rupesh',
 '006': 'nag'}

In [88]: emp_db.setdefault('007',"swetha")
Out[88]: 'swetha'

In [89]: emp_db
Out[89]: 
{'001': 'swaroopa',
 '002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python'],
 '005': 'rupesh',
 '006': 'nag',
 '007': 'swetha'}

In [90]: emp_db['001'] = "sen" # crude way

In [91]: print (emp_db)
{'001': 'sen', '002': 'satya', '003': 'satya', '004': ['tuxfux', 'trainer', 'python'], '005': 'rupesh', '006': 'nag', '007': 'swetha'}

In [92]: 


In [93]: #emp_db.pop

In [94]: emp_db.pop?
Docstring:
D.pop(k[,d]) -> v, remove specified key and return the corresponding value.
If key is not found, d is returned if given, otherwise KeyError is raised
Type:      builtin_function_or_method

In [95]: emp_db.pop('001')
Out[95]: 'sen'

In [96]: emp_db
Out[96]: 
{'002': 'satya',
 '003': 'satya',
 '004': ['tuxfux', 'trainer', 'python'],
 '005': 'rupesh',
 '006': 'nag',
 '007': 'swetha'}

In [97]: emp_db.pop('008')
Traceback (most recent call last):

  File "<ipython-input-97-c01245861908>", line 1, in <module>
    emp_db.pop('008')

KeyError: '008'


In [99]: #emp_db.popitem

In [100]: emp_db.popitem?
     ...: 
Docstring:
D.popitem() -> (k, v), remove and return some (key, value) pair as a
2-tuple; but raise KeyError if D is empty.
Type:      builtin_function_or_method

In [101]: emp_db.popitem()
Out[101]: ('007', 'swetha')

In [102]: emp_db.popitem()
Out[102]: ('006', 'nag')

In [103]: emp_db.popitem()
Out[103]: ('005', 'rupesh')

In [104]: emp_db
Out[104]: {'002': 'satya', '003': 'satya', '004': ['tuxfux', 'trainer', 'python']}

In [105]: 


In [106]: #emp_db.clear

In [107]: emp_db.clear?
Docstring: D.clear() -> None.  Remove all items from D.
Type:      builtin_function_or_method

In [108]: emp_db.clear()

In [109]: emp_db
Out[109]: {}

In [110]: 

## set

In [111]: set?
Init signature: set(self, /, *args, **kwargs)
Docstring:     
set() -> new empty set object
set(iterable) -> new set object

Build an unordered collection of unique elements.
Type:           type
Subclasses:     LazySet, LazySet, LazySet

In [112]: my_list = ["host1","host2","host3","host4","host1"]

In [113]: len(my_list)
Out[113]: 5

In [114]: set(my_list)
Out[114]: {'host1', 'host2', 'host3', 'host4'}

In [115]: unique_list = set(my_list)

In [116]: print (unique_list)
{'host4', 'host1', 'host3', 'host2'}

In [117]:

n [118]: a = {}

In [119]: print (a,type(a))
{} <class 'dict'>

In [120]: b = set()

In [121]: print (b,type(b))
set() <class 'set'>

In [122]:

 