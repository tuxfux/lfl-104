#!/usr/bin/python
# -*- coding: utf-8 -*-
# http://www.pythonchallenge.com/pc/def/map.html

'''
import string
string.ascii_lowercase

Logically speaking

A -> c
b -> d
c -> e
g -> i
x -> z
y -> a
z -> b

'''

my_string = """
g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. 
bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. 
sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.
"""

