#!/usr/bin/python
# -*- coding: utf-8 -*-

#Using While loop
import random
num=random.randint(1,10)
i=0

while i<3:
    x = int(input("Guess the number"))
    if x==num:
        print("Correct")
        break
    elif x>num:
        print("biggest")
    elif x<num:
        print("smaller")
    i=i+1
else:
    print("out of options")

