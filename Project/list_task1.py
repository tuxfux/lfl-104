#!/usr/bin/python
# -*- coding: utf-8 -*-

## 
my_days = ['yesterday','today','tomorrow','dayafter']

# task1
# output
#  yesterday 9
#  today     5
#  tomorrow  8
#  dayafter  8

for value in my_days:
    print (value,len(value))

# task2
# output
# Yesterday
# TOday
# TOMorrow
# DAYAfter

'''
In [87]: my_days = ['yesterday','today','tomorrow','dayafter']

In [88]: for value in my_days:
    ...:     print (value,my_days.index(value))
    ...:     
yesterday 0
today 1
tomorrow 2
dayafter 3

In [89]: for value in my_days:
    ...:     print (value,my_days.index(value),value[my_days.index(value)])
    ...:     
yesterday 0 y
today 1 o
tomorrow 2 m
dayafter 3 a

In [90]: for value in my_days:
    ...:     print (value,my_days.index(value),value[0:my_days.index(value)])
    ...:     
yesterday 0 
today 1 t
tomorrow 2 to
dayafter 3 day

In [91]: my_string = "python"

In [92]: my_string[0:0]
Out[92]: ''

In [93]: my_string[0:1]
Out[93]: 'p'

In [94]: for value in my_days:
    ...:     print (value,my_days.index(value),value[0:my_days.index(value)] + 1)
    ...:     
Traceback (most recent call last):

  File "<ipython-input-94-ed5768b99fd1>", line 2, in <module>
    print (value,my_days.index(value),value[0:my_days.index(value)] + 1)

TypeError: can only concatenate str (not "int") to str


In [95]: 

In [95]: for value in my_days:
    ...:     print (value,my_days.index(value),value[0:my_days.index(value) + 1])
    ...:     
yesterday 0 y
today 1 to
tomorrow 2 tom
dayafter 3 daya

In [96]:
    
In [96]: for value in my_days:
    ...:     print (value[0:my_days.index(value) + 1])
    ...:     
y
to
tom
daya

In [97]: my_string = "python"

In [98]: my_string[0:3]
Out[98]: 'pyt'

In [99]: my_string[:3]
Out[99]: 'pyt'

In [100]: my_string[3:7]
Out[100]: 'hon'

In [101]: my_string[3:]
Out[101]: 'hon'

In [102]: my_string[:3] + my_string[3:]
Out[102]: 'python'

In [103]: for value in my_days:
     ...:     print (value[:my_days.index(value) + 1] + value[my_days.index(value) + 1:])
     ...:     
yesterday
today
tomorrow
dayafter

In [104]: for value in my_days:
     ...:     print (value[:my_days.index(value) + 1].upper() + value[my_days.index(value) + 1:])
     ...:     
Yesterday
TOday
TOMorrow
DAYAfter

In [105]:   
    
'''