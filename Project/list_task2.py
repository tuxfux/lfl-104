#!/usr/bin/python
# -*- coding: utf-8 -*-

## case 1 
#my_fruits = ['apple','banana','apple','banana','cherry','banana']
## case 2
my_fruits = ['apple','apple','banana','apple','banana','banana','cherry','banana']
# output
# my_fruits = ['apple','banana','cherry']
# my_dupli  = ['apple','banana']

# hint
# count,index,remove
# my_fruits is a object
# my_fruits[:] is another object without a name

my_dupli = []
for fruit in my_fruits[:]:
    if my_fruits.count(fruit) > 1:
        my_fruits.remove(fruit)
        if fruit not in my_dupli:
            my_dupli.append(fruit)
    
        
print (my_dupli)
print (my_fruits)

'''
In [47]: a = [1,2,3]

In [48]: print (id(a),id(a[0]),id(a[1]),id(a[2]))
2081502259144 140733228094272 140733228094304 140733228094336

##  import copy
##  b = copy.deepcopy(a)
In [49]: b = a[:]

In [50]: print (id(b),id(b[0]),id(b[1]),id(b[2]))
2081502320008 140733228094272 140733228094304 140733228094336

In [51]: a is b
Out[51]: False

'''