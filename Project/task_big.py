#!/usr/bin/python
# -*- coding: utf-8 -*-
# and and or -> logical operators

num1 = input("please enter your num1:")
num2 = input("please enter your num2:")
num3 = input("please enter your num3:")

if num1 > num2 and num1 > num3:
    print ("{0} is greater than {1} and {2}".format(num1,num2,num3))
elif num2 > num1 and num2 > num3:
    print ("{1} is greater than {0} and {2}".format(num1,num2,num3))
elif num3 > num1 and num3 > num2:
    print ("{2} is greater than {0} and {1}".format(num1,num2,num3))
elif num1 == num2 and num1 == num3 and num2 == num3:
    print ("{0}  {1} and {2} are equal".format(num1,num2,num3))
elif num1 == num2 and num1 > num3:
    print ("{0} is greater number".format(num1))
else:
    print ("{0} is greater number".format(num3))

'''
## try to solve this logical issue
runfile('C:/Users/tuxfux/Documents/lfl-python104/big.py', wdir='C:/Users/tuxfux/Documents/lfl-python104')

please enter your num1:32

please enter your num2:32

please enter your num3:12
32  32 and 12 are equal
'''


