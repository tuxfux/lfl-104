Data structrues
 * Lists
 * tuples
 * dictionaries
 
Lists
------
* lists are linear representation of data.
* lists are mutable.
* Lists can be hetrogeneous ( numbers and strings )

lists vs arrays
----------------
https://www.scipy.org/

In [8]: # creation of a list

In [9]: my_fruits = ['apple','banana','cherry','dates']

In [11]: 

In [11]: print (my_fruits,type(my_fruits))
['apple', 'banana', 'cherry', 'dates'] <class 'list'>

In [13]: # list

In [14]: my_empty = list()

In [15]: print (my_empty,type(my_empty))
[] <class 'list'>

In [16]: my_empty = []

In [17]: print (my_empty,type(my_empty))
[] <class 'list'>

In [18]: 


In [24]: # in operator on list

In [25]: print (my_fruits)
['apple', 'banana', 'cherry', 'dates']

In [26]: 'apple' in my_fruits
Out[26]: True

In [27]: 'Apple' in my_fruits
Out[27]: False

In [28]: 


In [34]: # indexing and slicking

In [35]: my_fruits
Out[35]: ['apple', 'banana', 'cherry', 'dates']

In [36]: # ['apple', 'banana', 'cherry', 'dates']

In [37]: #    0         1           2       3     # +ve indexing on left to right

In [38]: #    -4        -3          -2      -1    # -ve indexing or right to left

In [39]: # banana and cherry

In [40]: my_fruits[1:3]
Out[40]: ['banana', 'cherry']

In [41]: my_fruits[-2:-4]
Out[41]: []

In [42]: my_fruits[-3:-1]
Out[42]: ['banana', 'cherry']

In [43]: my_fruits[-3:3]
Out[43]: ['banana', 'cherry']

In [44]: my_fruits[2]
Out[44]: 'cherry'

In [45]: 

In [46]: # mutables and immutable

In [47]: my_fruits
Out[47]: ['apple', 'banana', 'cherry', 'dates']

In [48]: my_fruits[0]
Out[48]: 'apple'

In [49]: my_fruits[0] = "Apple"

In [50]: my_fruits
Out[50]: ['Apple', 'banana', 'cherry', 'dates']

In [51]: my_string = "python"

In [52]: my_string[0]
Out[52]: 'p'

In [53]: my_string[0] = "T"
Traceback (most recent call last):

  File "<ipython-input-53-7317052dffb1>", line 1, in <module>
    my_string[0] = "T"

TypeError: 'str' object does not support item assignment


In [57]: # converting lists to string and vice-versa

In [58]: my_string = "python"

In [59]: print (my_string,type(my_string))
python <class 'str'>

# convert a string to a list

In [61]: Lmy_string = list(my_string)

In [62]: print (Lmy_string)
['p', 'y', 't', 'h', 'o', 'n']

## modified my list

In [63]: Lmy_string[0]
Out[63]: 'p'

In [64]: Lmy_string[0] = "T"

In [65]: print (Lmy_string)
['T', 'y', 't', 'h', 'o', 'n']

# convert the list back to a string


In [66]: limiter = ""

In [67]: limiter.join(Lmy_string)
Out[67]: 'Tython'

In [68]: limiter.join?
Signature: limiter.join(iterable, /)
Docstring:
Concatenate any number of strings.

The string whose method is called is inserted in between each given string.
The result is returned as a new string.

Example: '.'.join(['ab', 'pq', 'rs']) -> 'ab.pq.rs'
Type:      builtin_function_or_method

In [69]: limiter = ","

In [70]: limiter.join(Lmy_string)
Out[70]: 'T,y,t,h,o,n'

In [71]: limiter = ""

In [72]: limiter.join(Lmy_string)
Out[72]: 'Tython'

In [73]: new_string = limiter.join(Lmy_string)

In [74]: print (my_string,new_string)
python Tython

In [75]: 


In [76]: my_sentence = "today is sunday"

In [77]: # convert a string to a list

In [79]: Lmy_sentence= list(my_sentence)

In [80]: print (Lmy_sentence)
['t', 'o', 'd', 'a', 'y', ' ', 'i', 's', ' ', 's', 'u', 'n', 'd', 'a', 'y']

In [81]: # string - split

In [82]: my_sentence.split?
Signature: my_sentence.split(sep=None, maxsplit=-1)
Docstring:
Return a list of the words in the string, using sep as the delimiter string.

sep
  The delimiter according which to split the string.
  None (the default value) means split according to any whitespace,
  and discard empty strings from the result.
maxsplit
  Maximum number of splits to do.
  -1 (the default value) means no limit.
Type:      builtin_function_or_method

In [83]: my_sentence.split()
Out[83]: ['today', 'is', 'sunday']

In [84]: Lmy_sentence = my_sentence.split()
    ...: 

In [85]: print (Lmy_sentence)
['today', 'is', 'sunday']

In [86]: ## modify

In [87]: Lmy_sentence[-1] = "Monday"

In [88]: print (Lmy_sentence)
['today', 'is', 'Monday']

In [89]: ## join the list to a sentence

In [90]: " ".join(Lmy_sentence)
Out[90]: 'today is Monday'

In [91]: new_sentence = " ".join(Lmy_sentence)

In [92]: print (new_sentence)
today is Monday

In [93]: print (my_sentence)
today is sunday


### functions

Python 3.7.3 (default, Apr 24 2019, 15:29:51) [MSC v.1915 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 7.6.1 -- An enhanced Interactive Python.

In [1]: my_fruits = ['apple','banana','cherry','dates']

In [2]: #my_fruits.append

In [3]: my_fruits.append?
Signature: my_fruits.append(object, /)
Docstring: Append object to the end of the list.
Type:      builtin_function_or_method

In [4]: my_fruits.append("eggfruit")

In [5]: my_fruits
Out[5]: ['apple', 'banana', 'cherry', 'dates', 'eggfruit']

In [6]: 

Python 3.7.3 (default, Apr 24 2019, 15:29:51) [MSC v.1915 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 7.6.1 -- An enhanced Interactive Python.

In [1]: my_fruits = ['apple','banana','cherry','dates']

In [2]: #my_fruits.append

In [3]: my_fruits.append?
Signature: my_fruits.append(object, /)
Docstring: Append object to the end of the list.
Type:      builtin_function_or_method

In [4]: my_fruits.append("eggfruit")

In [5]: my_fruits
Out[5]: ['apple', 'banana', 'cherry', 'dates', 'eggfruit']

In [6]: # my_fruits.extend

In [7]: my_fruits.extend?
Signature: my_fruits.extend(iterable, /)
Docstring: Extend list by appending elements from the iterable.
Type:      builtin_function_or_method

In [8]: my_fruits.extend(["fig","gauva","hackberry"])

In [9]: my_fruits
Out[9]: ['apple', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry']

In [10]: 

In [11]: # my_fruits.insert

In [12]: my_fruits.insert?
Signature: my_fruits.insert(index, object, /)
Docstring: Insert object before index.
Type:      builtin_function_or_method

In [13]: my_fruits
Out[13]: ['apple', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry']

In [14]: my_fruits.insert(1,"avacado")

In [15]: my_fruits
Out[15]: 
['apple',
 'avacado',
 'banana',
 'cherry',
 'dates',
 'eggfruit',
 'fig',
 'gauva',
 'hackberry']
 
 

In [17]: #my_fruits.index

In [18]: my_fruits.index?
Signature: my_fruits.index(value, start=0, stop=9223372036854775807, /)
Docstring:
Return first index of value.

Raises ValueError if the value is not present.
Type:      builtin_function_or_method

In [19]: print (my_fruits)
['apple', 'avacado', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry']

In [20]: my_fruits.index("cherry")
Out[20]: 3

In [21]: # my_fruits.count

In [22]: my_fruits.count?
Signature: my_fruits.count(value, /)
Docstring: Return number of occurrences of value.
Type:      builtin_function_or_method

In [23]: my_fruits.count("cherry")
Out[23]: 1

In [24]: ## misc use cases

In [25]: my_fruits.append("apple")

In [26]: print (my_fruits)
['apple', 'avacado', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry', 'apple']

In [27]: my_fruits.index("apple")
Out[27]: 0

In [28]: my_fruits.index("apple",1)
Out[28]: 9

In [29]: ## count

In [30]: my_fruits.count("apple")
Out[30]: 2

In [31]: my_fruits.count("apple",/)
  File "<ipython-input-31-7f18cf5fa166>", line 1
    my_fruits.count("apple",/)
                            ^
SyntaxError: invalid syntax


In [32]: 

In [32]: my_fruits.index??
Signature: my_fruits.index(value, start=0, stop=9223372036854775807, /)
Docstring:
Return first index of value.

Raises ValueError if the value is not present.
Type:      builtin_function_or_method

In [33]: 

## TODO - what is / in the index and count

In [34]: #my_fruits.reverse

In [35]: my_fruits.reverse?
Signature: my_fruits.reverse()
Docstring: Reverse *IN PLACE*.
Type:      builtin_function_or_method

In [36]: print (my_fruits)
['apple', 'avacado', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry', 'apple']

In [37]: my_fruits.reverse()

In [38]: print (my_fruits)
['apple', 'hackberry', 'gauva', 'fig', 'eggfruit', 'dates', 'cherry', 'banana', 'avacado', 'apple']

In [39]: # sort

In [40]: my_fruits.sort?
Signature: my_fruits.sort(*, key=None, reverse=False)
Docstring: Stable sort *IN PLACE*.
Type:      builtin_function_or_method

In [41]: my_fruits.sort()


In [43]: 

In [43]: print (my_fruits)
['apple', 'apple', 'avacado', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry']


In [43]: 

In [43]: print (my_fruits)
['apple', 'apple', 'avacado', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry']


In [45]: 

In [45]: my_fruits.sort(reverse=False)

In [46]: print (my_fruits)
['apple', 'apple', 'avacado', 'banana', 'cherry', 'dates', 'eggfruit', 'fig', 'gauva', 'hackberry']


In [48]: 

In [48]: my_fruits.sort(reverse=True)

In [49]: print (my_fruits)
['hackberry', 'gauva', 'fig', 'eggfruit', 'dates', 'cherry', 'banana', 'avacado', 'apple', 'apple']

In [50]: 

In [51]: #my_fruits.pop

In [52]: my_fruits.pop?
Signature: my_fruits.pop(index=-1, /)
Docstring:
Remove and return item at index (default last).

Raises IndexError if list is empty or index is out of range.
Type:      builtin_function_or_method

In [53]: print (my_fruits)
['hackberry', 'gauva', 'fig', 'eggfruit', 'dates', 'cherry', 'banana', 'avacado', 'apple', 'apple']

In [54]: my_fruits.pop()
Out[54]: 'apple'

In [55]: print (my_fruits)
['hackberry', 'gauva', 'fig', 'eggfruit', 'dates', 'cherry', 'banana', 'avacado', 'apple']

In [56]: my_fruits.pop(1)
Out[56]: 'gauva'

In [57]: print (my_fruits)
['hackberry', 'fig', 'eggfruit', 'dates', 'cherry', 'banana', 'avacado', 'apple']

In [58]:


In [59]: my_fruits.remove?
Signature: my_fruits.remove(value, /)
Docstring:
Remove first occurrence of value.

Raises ValueError if the value is not present.
Type:      builtin_function_or_method

In [60]: print (my_fruits)
    ...: 
['hackberry', 'fig', 'eggfruit', 'dates', 'cherry', 'banana', 'avacado', 'apple']

In [61]: my_fruits.remove('eggfruit')

In [62]: print (my_fruits)
['hackberry', 'fig', 'dates', 'cherry', 'banana', 'avacado', 'apple']

In [63]: my_fruits.remove('eggfruit')
Traceback (most recent call last):

  File "<ipython-input-63-032aa48bcf14>", line 1, in <module>
    my_fruits.remove('eggfruit')

ValueError: list.remove(x): x not in list


In [65]: print (my_fruits)
['hackberry', 'fig', 'dates', 'cherry', 'banana', 'avacado', 'apple']

In [66]: my_fruits.append('apple')

In [67]: my_fruits.append('apple')

In [68]: print (my_fruits)
['hackberry', 'fig', 'dates', 'cherry', 'banana', 'avacado', 'apple', 'apple', 'apple']

In [69]: my_fruits.insert(2,'apple')

In [70]: print (my_fruits)
['hackberry', 'fig', 'apple', 'dates', 'cherry', 'banana', 'avacado', 'apple', 'apple', 'apple']

In [71]: my_fruits.remove('apple')

In [72]: print (my_fruits)
['hackberry', 'fig', 'dates', 'cherry', 'banana', 'avacado', 'apple', 'apple', 'apple']

In [73]: 

In [74]: #my_fruits.clear

In [75]: my_fruits.clear?
Signature: my_fruits.clear()
Docstring: Remove all items from list.
Type:      builtin_function_or_method

In [76]: my_fruits.clear()

In [77]: print (my_fruits)
[]

In [78]: 

## how list copy works - soft,deep copy

# is

In [10]: # is

In [11]: a = 1

In [12]: id(a)
Out[12]: 140727742010176

In [13]: id?
Signature: id(obj, /)
Docstring:
Return the identity of an object.

This is guaranteed to be unique among simultaneously existing objects.
(CPython uses the object's memory address.)
Type:      builtin_function_or_method

In [14]:

In [14]: id(1)
Out[14]: 140727742010176

In [15]: b = 1

In [16]: id(b)
Out[16]: 140727742010176

n [17]: b = 2

In [18]: id(b)
Out[18]: 140727742010208

In [19]: id(2)
Out[19]: 140727742010208

In [20]: a
Out[20]: 1

In [21]: b
Out[21]: 2

In [22]: id(a)
Out[22]: 140727742010176



In [23]: ## time to google

In [24]: z = 500

In [25]: y = 500

In [26]: id(z)
Out[26]: 1744693474512

In [27]: id(y)
Out[27]: 1744693476368

In [28]: z is y
Out[28]: False

In [29]: a = 1

In [30]: c = 1

In [31]: id(a)
Out[31]: 140727742010176

In [32]: id(c)
Out[32]: 140727742010176

## lists

In [1]: a = [1,2,3]

In [2]: print (id(a),id(a[0]),id(a[1]),id(a[2]))
2081485849544 140733228094272 140733228094304 140733228094336

In [3]: print (a,a[0],a[1],a[2])
[1, 2, 3] 1 2 3

## = symbol actually assigns variables to the same memory
block. this feature is called as soft copy.

In [4]: b = a

In [5]: print (b,b[0],b[1],b[2])
[1, 2, 3] 1 2 3

In [6]: print (id(b),id(b[0]),id(b[1]),id(b[2]))
2081485849544 140733228094272 140733228094304 140733228094336


In [7]: b is a
Out[7]: True

a[0]
a[0] = 11

In [17]: print (b)
[11, 2, 3]

In [18]: print (a)
[11, 2, 3]

### deep copy

In [20]: print (a)
[11, 2, 3]

In [21]: print (id(a),id(a[0]),id(a[1]),id(a[2]))
2081485849544 140733228094592 140733228094304 140733228094336

In [22]: import copy

In [23]: copy.deepcopy?
Signature: copy.deepcopy(x, memo=None, _nil=[])
Docstring:
Deep copy operation on arbitrary Python objects.

See the module's __doc__ string for more info.
File:      c:\users\tuxfux\anaconda3\lib\copy.py
Type:      function

In [24]: c = copy.deepcopy(a)

In [25]: a is c
Out[25]: False

In [26]: print (id(a),id(a[0]),id(a[1]),id(a[2]))
2081485849544 140733228094592 140733228094304 140733228094336

In [27]: print (id(c),id(c[0]),id(c[1]),id(c[2]))
2081502160584 140733228094592 140733228094304 140733228094336

In [28]: print (a)
[11, 2, 3]

In [29]: print (b)
[11, 2, 3]

In [30]: 

In [31]: a[0] = 22

In [32]: print (a)
[22, 2, 3]

In [34]: print (c)
[11, 2, 3]

### how about if i modify whole list in a soft copied objects.

In [36]: print (a,a[0],a[1],a[2])
[22, 2, 3] 22 2 3

In [37]: print (id(a),id(a[0]),id(a[1]),id(a[2]))
2081485849544 140733228094944 140733228094304 140733228094336

In [38]: print (b,b[0],b[1],b[2])
[22, 2, 3] 22 2 3

In [39]: print (id(b),id(b[0]),id(b[1]),id(b[2]))
2081485849544 140733228094944 140733228094304 140733228094336

In [40]: a = ["a","b","c"]

In [41]: print (id(a),id(a[0]),id(a[1]),id(a[2]))
2081489835400 2081396260232 2081396258552 2081396125400

In [42]: print (a,a[0],a[1],a[2])
['a', 'b', 'c'] a b c

In [43]: print (b,b[0],b[1],b[2])
[22, 2, 3] 22 2 3

In [44]: print (id(b),id(b[0]),id(b[1]),id(b[2]))
2081485849544 140733228094944 140733228094304 140733228094336

In [45]: 

## other functions

In [55]: my_fruits = ['apple','banana','cherry','dates']

In [56]: my_fruits.copy?
Signature: my_fruits.copy()
Docstring: Return a shallow copy of the list.
Type:      builtin_function_or_method

In [57]: new_fruits = my_fruits.copy()

In [58]: print (new_fruits)
['apple', 'banana', 'cherry', 'dates']

In [59]: print (my_fruits)
['apple', 'banana', 'cherry', 'dates']

In [60]: new_fruits is my_fruits
Out[60]: False


## list comprehensions




In [65]: num = input("please enter your number:")

please enter your number:1,2,3,4,5,6,7,8,9,10

In [66]: print (num,type(num))
    ...: 
1,2,3,4,5,6,7,8,9,10 <class 'str'>

In [67]: ## even numbers in the num and print them out as string 

In [68]: ## 2,4,6,8,10 needs to be printed

In [69]: num.split?
Signature: num.split(sep=None, maxsplit=-1)
Docstring:
Return a list of the words in the string, using sep as the delimiter string.

sep
  The delimiter according which to split the string.
  None (the default value) means split according to any whitespace,
  and discard empty strings from the result.
maxsplit
  Maximum number of splits to do.
  -1 (the default value) means no limit.
Type:      builtin_function_or_method

In [70]: num.split(",")
Out[70]: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']

In [71]: for value in num.split(","):
    ...:     if int(value) % 2 == 0:
    ...:         print (value)
    ...:         
2
4
6
8
10

In [72]: my_int = []

In [74]: 


In [75]: for value in num.split(","):
    ...:     if int(value) % 2 == 0:
    ...:         print (value)
    ...:         my_int.append(value)
    ...:         
2
4
6
8
10



In [77]: print (my_int)
['2', '4', '6', '8', '10']

In [78]: 

In [79]: ",".join(my_int)
Out[79]: '2,4,6,8,10'

In [80]: my_output = ",".join(my_int)

In [81]: print (my_output)
2,4,6,8,10


## example 1

In [83]: ## list comprehnesion

In [84]: # [ print value; loop ; condition  ] 

In [85]: [ value for value in num.split(",") ]
Out[85]: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']

In [86]: [ value for value in num.split(",") if int(value) % 2 == 0]
Out[86]: ['2', '4', '6', '8', '10']

In [87]: ",".join([ value for value in num.split(",") if int(value) % 2 == 0])
Out[87]: '2,4,6,8,10'

## example 2

In [89]: my_string = "today is friday"

## output
# [['today', 'TODAY', 'Today'],
# ['is', 'IS', 'Is'],
# ['friday', 'FRIDAY', 'Friday']]

In [92]: [ [value] for value in my_string.split()]
Out[92]: [['today'], ['is'], ['friday']]

In [93]: [ [value,value.upper(),value.capitalize()] for value in my_string.split()]
Out[93]: 
[['today', 'TODAY', 'Today'],
 ['is', 'IS', 'Is'],
 ['friday', 'FRIDAY', 'Friday']]

In [94]: 

